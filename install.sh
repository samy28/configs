#! /usr/bin/bash

sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc && dnf copr enable evana/fira-code-fonts && sudo dnf update && sudo dnf install < fedora-packages.txt && sudo dnf config-manager --add-repo https://repo.vivaldi.com/archive/vivaldi-fedora.repo && sudo dnf update && sudo dnf install vivaldi-stable && git clone --depth 1 https://github.com/hlissner/doom-emacs ~/.emacs.d && ~/.emacs.d/bin/doom install
