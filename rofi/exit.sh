#!/usr/bin/env bash

DMENU="rofi -theme flamingo -dmenu"

declare -a options=(
"Lock"
"Logout"
"Reboot"
"Shut Down"
"Quit"
)

selection=$(printf '%s\n' "${options[@]}" | $DMENU -p "Careful, bud!")

case "$selection" in
    Lock) xscreensaver-command -lock;;
    Logout) loginctl kill-session $XDG_SESSION_ID;;
    Reboot) reboot;;
    "Shut Down") poweroff;;
    Quit) echo "Sayonara" && exit 1
esac
