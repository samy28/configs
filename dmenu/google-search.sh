#!/usr/bin/env bash

DMENU="dmenu -i -l 20 -fn Poppins -nb #282c34 -nf #bbc2cf -sb #ecbe7b -sf #282c34"
BROWSER="firefox"

while [[ -z "$selection" ]]; do
    selection=$(printf '%s' | $DMENU -p "Google Search") || exit
done

$BROWSER "https://www.google.com/search?q="$selection
