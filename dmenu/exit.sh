#!/usr/bin/env bash

DMENU="dmenu -i -fn Poppins -nb #282c34 -nf #bbc2cf -sb #ff6c6b -sf #282c34"

declare -a options=(
"Lock"
"Logout"
"Reboot"
"Shut Down"
"Quit"
)

selection=$(printf '%s\n' "${options[@]}" | $DMENU -p "Careful, bud!")

case "$selection" in
    Lock) xscreensaver-command -lock;;
    Logout) loginctl kill-session $XDG_SESSION_ID;;
    Reboot) reboot;;
    "Shut Down") poweroff;;
    Quit) echo "Sayonara" && exit 1
esac
