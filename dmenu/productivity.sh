#!/usr/bin/env bash

DMENU="dmenu -i -fn Poppins -nb #282c34 -nf #bbc2cf -sb #a9a1e1 -sf #282c34"
EDITOR="emacsclient -c -a emacs"

declare -a options=(
    "Add/Remove TODO Task"
    "View TODO List"
    "View Agenda"
    "Quit"
)

selection=$(printf '%s\n' "${options[@]}" | $DMENU -p "Good Luck for the Day !")

case "$selection" in
    "Add/Remove TODO Task") $EDITOR $HOME/Documents/agenda.org;;
    "View TODO List") $EDITOR -e '(org-todo-list)';;
    "View Agenda") emacs --daemon || $EDITOR -e '(org-agenda-list)';;
    Quit) echo "Sayonara" && exit 1
esac
