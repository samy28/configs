#!/usr/bin/env bash

DMENU="dmenu -i -l 20 -fn Poppins -nb #282c34 -nf #bbc2cf -sb #51afef -sf #282c34"
EDITOR="emacsclient -c -a emacs"

declare -a options=(
"BASH - $HOME/.bashrc"
"ZSH - $HOME/.zshrc"
"FISH - $HOME/.config/fish/config.fish"
"VIM - $HOME/.vimrc"
"AWESOMEWM - $HOME/.config/awesome/rc.lua"
"LEFTWM - $HOME/.config/leftwm"
"XMONAD - $HOME/.xmonad/xmonad.hs"
"BSPWM - $HOME/.config/bspwm/bspwmrc"
"SXHKD - $HOME/.config/sxhkd/sxhkdrc"
"POLYBAR - $HOME/.config/polybar/config"
"XMOBAR - $HOME/.config/xmobar/xmobarrc"
"PICOM - $HOME/.config/picom/picom.sample.conf"
"ALACRITTY - $HOME/.config/alacritty/alacritty.yml"
"DOOM EMACS init.el - $HOME/.config/doom/init.el"
"DOOM EMACS config.el - $HOME/.config/doom/config.el"
"DOOM EMACS packages.el - $HOME/.config/doom/packages.el"
"QUIT"
)

selection=$(printf '%s\n' "${options[@]}" | $DMENU -p "Careful, bud!")

if [[ "$selection" == "QUIT" ]]; then
    echo "Sayonara" && exit 1

elif [[ "$selection" ]]; then
    config=$(printf '%s\n' "${selection}" | awk '{print $NF}')
    $EDITOR $config

else
    echo "Ariosumigo" && exit 1
fi
